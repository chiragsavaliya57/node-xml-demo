# File watcher
A node.js app that watchs a folder and on every new or changed file there calls AWS Lambda fuction.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine.

## Prerequisites
Node.JS 10+ (https://nodejs.org/en/download/)

## Installing
1. Clone or download the repository and open it in the command line.
2. Install dependencies running ```npm install```.
3. Open **index.js** in the root and specify values for **WATCH_FOLDER** and **AWS_LAMBDA_URL** variables.
    **WATCH_FOLDER** - path to the folder that you want to watch (use / as a delimeter in Windows).
    **AWS_LAMBDA_URL** - URL of the AWS Lambda that is going to be called.

## Running
Run ```node index.js``` in the root.

## Output
Output of the app is a response from AWS or error message if something went wrong.

## Notes
If the specified folder already contains files you'll immediately see messages in the command line.