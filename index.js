const chokidar = require('chokidar');
const fs = require('fs');
const fetch = require('node-fetch');

// change if needed
const WATCH_FOLDER = '.';
const AWS_LAMBDA_URL = 'https://krmf3d99lj.execute-api.us-east-1.amazonaws.com/dev/hello-world';

const watcher = chokidar.watch(process.env.WATCH_PATH || WATCH_FOLDER, { depth: 50 });

watcher
  .on('add', handleFile)
  .on('change', handleFile)

function handleFile(path) {
    fs.readFile(path, 'utf-8', (err, data) => {
        if (err) {
            return;
        }

        if (data.length) {
            callLambda(data, (err, content) => {
                if (err) {
                    return;
                }
    
                console.log(content);
            });
        }
    })
}

function callLambda(fileContent, callback) {
    fetch(process.env.AWS_LAMBDA_URL || AWS_LAMBDA_URL, {
        method: 'post',
        body: fileContent,
        headers: { 'Content-Type': 'application/xml' },
    })
    .then(checkStatus)
    .then(res => res.text())
    .then(content => callback(null, content))
    .catch(err => callback(err));
}

function checkStatus(res) {
    if (res.ok) {
        return res;
    }

    throw new Error();
}